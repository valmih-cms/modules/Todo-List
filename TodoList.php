<?php

class TodoList extends Module {
	function install() {
		$this->connection->createTable("todo_list",
            ["id", "userId", "title", "description", "parentId", "date", "status"],
			["integer", "integer", "varchar(50)", "varchar(100)", "integer", "integer", "varchar(100)"]);
	}

	function addTask($title, $description, $parentId, $status, $date) {
		assert (isset($_SESSION["user"]));
		$userId = $this->connection->getUserID($_SESSION["user"]);
		$query = <<<EOF
		INSERT INTO todo_list (userId, title, description, parentId, status, date)
			VALUES ($userId, '$_POST[newTask]', NULL, -1, 'WIP', $date)
		EOF;
		$this->connection->query($query);
	}

	function updateTaskStatus($taskId, $newStatus) {
		$query = <<<EOF
		UPDATE todo_list SET status='$newStatus' WHERE id=$_POST[taskId]
		EOF;
		$this->connection->query($query);
	}

	function getDayTasks($day) {
		assert (isset($_SESSION["user"]));
		$userId = $this->connection->getUserID($_SESSION["user"]);
		$strDay = $day->format("Ymd") . "000000";
		$strEndDay = $day->format("Ymd") . "235959";

		$query = <<<EOF
			SELECT * FROM todo_list where userID=$userId AND date>=$strDay AND date<$strEndDay
		EOF;
		$todayTasks = $this->connection->query($query)->fetchArray("assoc");
		return $todayTasks;
	}

	function getCountEntriesInInterval($startDate, $endDate) {
		assert (isset($_SESSION["user"]));
		$userId = $this->connection->getUserID($_SESSION["user"]);

		$strStartDate = $startDate->format("Ymd") . "000000";
		$strEndDate = $endDate->format("Ymd") . "235959";
		$query = <<<EOF
		SELECT * FROM todo_list where userID=$userId AND date>=$strStartDate AND date<=$strEndDate;
		EOF;
		$res = $this->connection->query($query)->fetchArray("assoc");

		$days = [];
		$day = $startDate;
		while($day->format("Ymd") != $endDate->format("Ymd")) {
			$days[$day->format("Ymd")] = ["DONE" => 0, "WIP" => 0];
			$day = $day->add(new DateInterval("P1D"));
		}
		$days[$day->format("Ymd")] = ["DONE" => 0, "WIP" => 0];

		$res2 = [];
		for($i=0; $i<count($res); $i++) {
			$item = strval($res[$i]["date"]);
			$item = substr($item, 0, -6);
			$status = $res[$i]["status"];
			$days[$item][$status] += 1;
		}
		return $days;
	}

}

?>